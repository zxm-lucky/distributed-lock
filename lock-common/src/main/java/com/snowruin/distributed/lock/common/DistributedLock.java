package com.snowruin.distributed.lock.common;

/**
 * 分布式锁接口
 * @author zxm
 * @version 1.0.0
 * @date 2019-03-01 11:44:07
 * @url https://www.snowruin.com
 *
 */

public interface DistributedLock {
	
	/**
	 * 获得锁超时时间
	 */
	public static final long TIMEOUT_MILLIS = 30000;
	
	/**
	 * 重试次数
	 */
	public static final int RETRY_TIMES = Integer.MAX_VALUE;
	
	/**
	 * 间隔时间
	 */
	public static final long SLEEP_MILLIS = 500;
	
	boolean lock(String keyName);
	
	boolean lock(String keyName,int retryTimes);
	
	boolean lock(String keyName,int retryTimes,long sleepMillis);
	
	boolean lock(String keyName,long expire);
	
	boolean lock(String keyName,long expire,int retryTimes);
	
	boolean lock(String keyName,long expire,int retryTimes,long sleepMillis);
	
	/**
	 * 释放锁
	 * @param keyName
	 * @return
	 */
	boolean releaseLock(String keyName);
	

}
