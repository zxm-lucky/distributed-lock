package com.snowruin.redis.lock.enums;

/**
 * 获取锁失败后的动作
 * @author zxm
 * @version 1.0.0
 * @date 2019-03-02 16:35:00
 * @url https://www.snowruin.com
 */
public enum FailHandler {

	/**
	 * 放弃
	 */
	GIVEUP,
	
	/**
	 * 继续
	 */
	CONTINUE;
	
}
