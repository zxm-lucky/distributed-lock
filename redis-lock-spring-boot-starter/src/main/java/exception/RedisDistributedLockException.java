package exception;

/**
 * redis 分布式锁  异常
 * @author zxm
 * @version 1.0.0
 * @date 2019-03-05 11:28:22
 * @url https://www.snowruin.com
 *
 */
public class RedisDistributedLockException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5625717782730289358L;

	
	
	public RedisDistributedLockException(){
		this(null);
	}
	
	
	public RedisDistributedLockException(String message) {
		this(message, null);
	}

	
	public RedisDistributedLockException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
}
