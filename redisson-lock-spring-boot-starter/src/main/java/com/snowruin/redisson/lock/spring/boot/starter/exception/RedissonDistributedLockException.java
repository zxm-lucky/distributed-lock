package com.snowruin.redisson.lock.spring.boot.starter.exception;

/**
 * 异常
 * @author zxm
 * @date   2019年3月14日
 * @url    https://www.snowruin.com
 *
 */
public class RedissonDistributedLockException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RedissonDistributedLockException() {
		super();
	}
	
	public RedissonDistributedLockException (String message) {
		super(message);
	}
	
	public RedissonDistributedLockException (String message,Throwable throwable) {
		super(message, throwable);
	}
}
