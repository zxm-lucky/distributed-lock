package com.snowruin.redisson.lock.spring.boot.starter.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

import org.springframework.core.annotation.AliasFor;

import com.snowruin.redisson.lock.spring.boot.starter.enums.RedissonLockType;


/**
 * redisson 分布式锁的处理
 * @author zxm
 * @date   2019年3月14日
 * @url    https://www.snowruin.com
 *
 */
@Target(value = ElementType.METHOD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface RedissonLockHandler {
	
	/**
	 * 锁资源。 key 。 支持 SPEl 表达式
	 * @return
	 */
	@AliasFor("key")
	String value() default  "default";
	
	@AliasFor("value")
	String key() default "default";
	 
	/**
	 * 锁类型  默认   可重入锁
	 * @return
	 */
	RedissonLockType type() default RedissonLockType.REENTRANT_LOCK;

	/**
	 * 获取锁的等待时间
	 * @return
	 */
	long waitTIme () default 3000l;
	
	/**
	 * 释放锁的时间
	 * @return
	 */
	long leaseTime() default 30000l;
	
	/**
	 * 获取所的时间单位
	 * @return
	 */
	TimeUnit timeUnit() default TimeUnit.MILLISECONDS;
}
