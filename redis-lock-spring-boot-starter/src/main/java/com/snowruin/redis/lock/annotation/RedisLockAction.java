package com.snowruin.redis.lock.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import com.snowruin.redis.lock.enums.FailHandler;

/**
 * redis 锁的获取规则
 * @author zxm
 * @version 1.0.0
 * @date 2019-03-04 11:17:00
 * @url https://www.snowruin.com
 */
@Target(value = { ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisLockAction {

	/**
	 * 锁资源，支持 spring el 表达式
	 * @return
	 */
	@AliasFor("key")
	String value() default "default" ;
	
	
	@AliasFor("value")
	String key() default "default";
	
	/**
	 * 持锁时长（毫秒）
	 * @return
	 */
	long keepMillis() default 3000l;
	
	/**
	 * 获取失败后的动作，默认继续
	 * @return
	 */
	FailHandler handler() default FailHandler.CONTINUE;
	
	/**
	 * 获取失败后重试的间隔时间，默认200毫秒
	 * @return
	 */
	long sleepMillis() default 200;

	
	/**
	 * 获取失败后重试的次数 ， 默认5次
	 * @return
	 */
	int retryCount() default 5;
}
