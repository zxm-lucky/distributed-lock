# distributed-lock


基于spring boot 2.1.2 RELEASE  实现的redis 分布式锁。 为兼容多方式的使用。实现了注解式 和 编程式 引用。 
#### 注解式： 在目标方法上添加  @RedisLockAction 注解 。 里面的key 可支持  SpEL 表达式。
#### 编程式： 将 DistributedLock 注入到你自己的Bean 中即可。 如： @Autowired private DistributedLock distributedLock;
  
  
 Maven依赖方式：
#### 	 &lt;dependency&gt;
####		&lt;groupId&gt;com.snowruin&lt;/groupId&gt;
####		&lt;artifactId&gt;redis-lock-spring-boot-starter&lt;/artifactId&gt;
####		&lt;version&gt;1.0&lt;/version&gt;
####	&lt;/dependency&gt;
	

后续会有其他的实现方式.