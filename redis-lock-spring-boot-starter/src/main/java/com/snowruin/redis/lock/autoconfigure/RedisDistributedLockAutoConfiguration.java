package com.snowruin.redis.lock.autoconfigure;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import com.snowruin.distributed.lock.common.DistributedLock;
import com.snowruin.redis.lock.ReidsDistributedLock;

/**
 * 在 RedisAutoConfiguration 初始化后 创建 DistributedLockAutoConfiguration 类型的 bean
 * @author zxm
 * @version 1.0.0
 * @date 2019-03-02 15:17:00
 * @url https://www.snowruin.com
 */

@Configuration
@AutoConfigureAfter(value=RedisAutoConfiguration.class)
public class RedisDistributedLockAutoConfiguration {

	/**
	 * redisTemplate Bean 存在时创建该bean
	 * @param redisTemplate
	 * @return
	 */
	@Bean
	@ConditionalOnBean(value = RedisTemplate.class)
	public DistributedLock redisDistributedLock(RedisTemplate<String, Object> redisTemplate) {
		return new ReidsDistributedLock(redisTemplate);
	}
	
}
