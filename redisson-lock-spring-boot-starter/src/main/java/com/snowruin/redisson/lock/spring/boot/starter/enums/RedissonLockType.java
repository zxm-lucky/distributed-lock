package com.snowruin.redisson.lock.spring.boot.starter.enums;

/**
 * 锁类型
 * @author zxm
 * @date   2019年3月14日
 * @url    https://www.snowruin.com
 *
 */
public enum RedissonLockType {

	/**
	 * 可重入锁
	 */
	REENTRANT_LOCK, 
	
	/**
	 * 公平锁
	 */
	FAIR_LOCK,
	
	/**
	 * 写锁
	 */
	WRITE_LOCK,
	
	/**
	 * 读锁
	 */
	READ_LOCK;
	
}
