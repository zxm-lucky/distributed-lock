package com.snowruin.distributed.lock.common;

/**
 * 分布式锁抽象类
 * @author zxm
 * @version 1.0.0
 * @date 2019-03-01 11:46:16
 * @url https://www.snowruin.com
 *
 */

public abstract class AbstractDisibutedLock implements DistributedLock {

	public boolean lock(String keyName) {
		// TODO Auto-generated method stub
		return lock(keyName, TIMEOUT_MILLIS, RETRY_TIMES, SLEEP_MILLIS);
	}

	public boolean lock(String keyName, int retryTimes) {
		// TODO Auto-generated method stub
		return lock(keyName, TIMEOUT_MILLIS, retryTimes, SLEEP_MILLIS);
	}

	public boolean lock(String keyName, int retryTimes, long sleepMillis) {
		// TODO Auto-generated method stub
		return lock(keyName, TIMEOUT_MILLIS, retryTimes, SLEEP_MILLIS);
	}

	public boolean lock(String keyName, long expire) {
		// TODO Auto-generated method stub
		return lock(keyName, expire, RETRY_TIMES, TIMEOUT_MILLIS);
	}

	public boolean lock(String keyName, long expire, int retryTimes) {
		// TODO Auto-generated method stub
		return lock(keyName, expire, retryTimes, SLEEP_MILLIS);
	}

}
